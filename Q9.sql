﻿select
	i2.category_name,
	sum(i.item_price) as total_price
from
	item i
inner join
	item_category i2
on
	i.category_id = i2.category_id
group by
	i.category_id
order by
	i.item_price desc
