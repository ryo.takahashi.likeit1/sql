﻿select
	i.item_id,
	i.item_name,
	i.item_price,
	i2.category_name
from
	item i
inner join
	item_category i2
on
	i.category_id = i2.category_id
